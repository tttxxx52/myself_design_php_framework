<?php
    require_once 'include/php/event_message.php';
    require_once 'include/php/PDO_mysql.php';
    require_once 'modules/module_management/module_management_api.php';
    require_once 'include/php/config_json.php';
    
    $body = (new Main())->run();
    echo $body;
    
    class Main{
        private $module;
        public function __construct(){
            $config = config_json::read("config.json.php");
            PDO_mysql::$db_host = $config["db"]["db_host"];
            PDO_mysql::$db_name = $config["db"]["db_name"];
            PDO_mysql::$db_user = $config["db"]["db_user"];
            PDO_mysql::$db_password = $config["db"]["db_password"];
            $this->module = $config["default_module"];
            $this->test_mode = $config['test_mode'];
            $this->version = $config['version'];
        }
        public function run(){
            $event_message = new event_message($_GET, $_POST);
            $get = $event_message->getGet();
            
            if(isset($_GET['module'])) 
                $module=$_GET['module'];
            else 
                $module= $this->module;    
            if($get['module'] !== 'login' || $get['action'] !== 'do_login_action'){
                try{
                    session_start();
                    if(!isset($_SESSION['user'])) throw new Exception("No user login.", 2);
                    $user = $_SESSION['user'];
                    if(!$this->test_mode){
                        $result = module_management_api::check_account_privilege($user, $get['module'], $get['action']);
                        if($result['status_code']!=0) throw new Exception ($result['status_message'], $result['status_code']);
                    }
                } catch (Exception $e){
                        $return_value['status_code'] = $e->getCode();
                        $return_value['status_message'] = $e->getMessage();
                        return json_encode($return_value);
                }
            }
                    
            require_once "modules" . "/" . $module. "/" . 'action_dispatcher.php';
            $module_object = new action_dispatcher();

            $body = $module_object->doAction($event_message);
            return $body;
        }
    }
    
?>
