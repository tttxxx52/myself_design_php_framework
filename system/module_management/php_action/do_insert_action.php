<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once 'include/php/PDO_mysql.php';
    
    class do_insert_action implements action_listener{
        public function actionPerformed(event_message $em) {
            $conn = PDO_mysql::getConnection();
            $post = $em->getPost();
            $name = $post['name'];
            $email = $post['email'];
            $tel = $post['tel'];
            $addr = $post['addr'];
            $sql = "INSERT INTO member_profile (name, email, tel, addr) VALUES (?, ?, ?, ?)";
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute(array($name, $email, $tel, $addr));
            if($result) 
                $msg = '新增成功';
            else
                $msg = "新增失敗";
            $body=$msg;
            return $body;
        }        
    }
  