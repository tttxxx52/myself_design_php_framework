<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once dirname(__FILE__, 4) . '/include/php/module_server_api.php';
    
    class new_module implements action_listener{
        public function actionPerformed(event_message $em) {
            $mlist = module_server_api::get_module_server_list();
            $return_value['status_code'] = 0;
            $return_value['status_message'] = 'Execute Success';
            $return_value['module_list'] = json_decode($mlist);
            return json_encode($return_value);
        }
    }
?>