<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    
    class show_privilege_page implements action_listener{
        public function actionPerformed(event_message $em) {
            $conn = PDO_mysql::getConnection();
            $sql = "SELECT module_action.id, module_profile.name, module_action.action_name, action_privilege.role_id FROM `module_profile`, `module_action`, `action_privilege` WHERE module_profile.id = module_action.module_id and module_action.id = action_privilege.action_id";
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute();
            if($result){
                $ds = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $return_value['error'] = 0;
                $return_value['error_message'] = 'Execute Success';
                $return_value['data_set'] = $ds;
            }
            else{
                $return_value['error'] = -1;
                $return_value['error_message'] = 'Execute Error';
                $return_value['sql'] = $sql;
            }
            return json_encode($return_value);
        }        
    }
?>