<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once dirname(__FILE__, 4) . '/include/php/module_server_api.php';
    
    class show_module_list implements action_listener{
        public function actionPerformed(event_message $em) {
            $conn = PDO_mysql::getConnection();
            //$sql = "SELECT module_profile.id as module_id, module_profile.name as module_name, module_profile.type as module_type, module_action.id as action_id, module_action.action_name as action_name FROM module_profile, module_action WHERE module_action.module_id=module_profile.id";
            $sql = "SELECT id as module_id, name as module_name, type as module_type, version as module_version FROM module_profile";
            $post = $em->getPost();
            $where_statement = $post['where_statement'];
            if($where_statement != ""){
                $sql .= " where $where_statement";
            }
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute();
            $mlist = json_decode(module_server_api::get_module_server_list());
            if($result){
                $ds = $stmt->fetchAll(PDO::FETCH_ASSOC);
                for($i=0; $i<count($ds); $i++) {
                    $module_name = $ds[$i]['module_name'];
                    if($module_name =='main_system'){
                        $config_file = 'config.json.php';
                    } else{
                        $config_file = 'modules/' . $module_name . '/config.json.php';
                    }
                    $config = config_json::read($config_file);
                    $ds[$i]['physical_version'] = $config['version'];
                    $ds[$i]['server_version'] = $mlist[$module_name]['module_version'];
                    //$ds[$i]['server_version'] = "1.11";
                }
                $return_value['status_code'] = 0;
                $return_value['status_message'] = 'Execute Success';
                $return_value['data_set'] = $ds;
                $return_value['sql'] = $sql;
                $return_value['mlist'] = $mlist;
                
            }
            else{
                $return_value['status_code'] = -1;
                $return_value['status_message'] = 'SQL Execute Error';
                $return_value['sql'] = $sql;
            }
            return json_encode($return_value);
        }
        private function get_module_list(){
            $folder    = './modules';
            $file_list = scandir($folder);
            $file_list = array_diff($file_list, array('.', '..'));
            return $file_list;
        }
    }
?>