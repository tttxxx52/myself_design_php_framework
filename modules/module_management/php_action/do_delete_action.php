<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once 'include/php/PDO_mysql.php';
    
    class do_delete_action implements action_listener{
        
        public function actionPerformed(event_message $em) {
            $conn = PDO_mysql::getConnection();
            $post = $em->getPost();
            $id = $post['id'];
            $sql = "DELETE FROM user_profile WHERE id=?";
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute(array($id));
            if($result) 
                $msg = '刪除成功';
            else
                $msg = "刪除失敗";
            return $msg;
        }
    }
?>
