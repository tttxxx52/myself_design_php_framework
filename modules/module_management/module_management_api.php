<?php
    //require_once 'php_action/do_select_action.php'; 會有namespace問題
    class module_management_api{
        static function check_account_privilege($user, $module, $action){
            $conn = PDO_mysql::getConnection();
            $sql = "SELECT role_user.role_id as role_id FROM `user_profile`, `role_user` WHERE user_profile.id = role_user.user_id and user_profile.email = '$user'";
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute();
            if($result){
                $user_roles = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
            else{
                $return_value['status_code'] = -1;
                $return_value['status_message'] = 'Execute Error';
                $return_value['sql'] = $sql;
                return json_encode($return_value);
            }
           
            $sql = "SELECT action_privilege.role_id as role_id ";
            $sql .= "FROM `module_profile`, `module_action`, `action_privilege` ";
            $sql .= "WHERE module_profile.id = module_action.module_id and module_action.id = action_privilege.action_id ";
            $sql .= "and module_profile.name = '$module' and module_action.action_name = '$action'";
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute();
            if($result){
                $action_roles = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($user_roles as $ur) {
                    foreach ($action_roles as $ar) {
                        if($ur['role_id'] === $ar['role_id']){
                            $return_value['status_code'] = 0;
                            $return_value['status_message'] = 'Execute Success';
                            return $return_value;
                        }
                    }
                }
                $return_value['status_code'] = 4;
                $return_value['status_message'] = 'Insufficient Privilege';
            }
            else{
                $return_value['status_code'] = -1;
                $return_value['status_message'] = 'Execute Error';
                $return_value['sql'] = $sql;
            }
            return $return_value;
        }
    }
?>