<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once 'modules/user_profile/user_profile_api.php';
    
    class do_login_action implements action_listener{
        public function actionPerformed(event_message $em) {
            $obj = user_profile_api::check_account($em);
            if($obj['status_code']!=0){
                return json_encode($obj);
            }
            session_start();
            $post = $em->getPost();
            $_SESSION['user'] = $obj['data_set'][0]['id'];
            return json_encode($obj);
        }        
    }
?>