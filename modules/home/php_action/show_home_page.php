<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once 'modules/user_profile/user_profile_api.php';
    
    class show_home_page implements action_listener{
        public function actionPerformed(event_message $em) {
            $return_value['status_code'] = 1;
            $return_value['user'] = $_SESSION['user'];
            $return_value['status_message'] = 'User has logon';
            return json_encode($return_value);
        }        
    }
?>