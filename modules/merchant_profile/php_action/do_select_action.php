<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    
    class do_select_action implements action_listener{
        public function actionPerformed(event_message $em) {
            $conn = PDO_mysql::getConnection();
            $sql = "SELECT * FROM merchant_profile";
            $post = $em->getPost();
            $where_statement = $post['where_statement'];
            if($where_statement != ""){
                $sql .= " where $where_statement";
            }
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute();
            if($result){
                $ds = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $return_value['error'] = 0;
                $return_value['error_message'] = 'Execute Success';
                $return_value['data_set'] = $ds;
            }
            else{
                $return_value['error'] = -1;
                $return_value['error_message'] = 'Execute Error';
                $return_value['sql'] = $sql;
            }
            return json_encode($return_value);
        }        
    }
?>