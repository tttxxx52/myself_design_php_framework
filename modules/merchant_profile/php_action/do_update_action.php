<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once 'include/php/PDO_mysql.php';
    
    class do_update_action implements action_listener{
        public function actionPerformed(event_message $em) {
            $post = $em->getPost();
            $id = $post['id'];
            $name = $post['name'];
            $contact_id = $post['contact_id'];
            $conn = PDO_mysql::getConnection();
            $sql = "UPDATE merchant_profile SET name=:name, contact_id=:contact_id WHERE id=:id";
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute(array(":name"=>$name, ":contact_id"=>$contact_id, ":id"=>$id));
            if($result) 
                $msg = '更新成功';
            else
                $msg = "更新失敗";
            return $msg;
        }    
    }
    
?>
